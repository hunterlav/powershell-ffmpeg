#target size in bits, but realistically it over shoots this by 3-4%
$MAX_SIZE=67108860

#Process each .mov or .mp4 file in the same folder as the script *recursively*
Get-ChildItem -Recurse -Include *.mov,*.mp4, *.mkv | ForEach-Object {
    #Use ffprobe to get the duration
    $duration = (ffprobe -v error -show_format $_.Name | Select-String "duration") -split("=") | Select-Object -Index 1
    
    #Calculate bitrate
    $bitrate = [int]($MAX_SIZE / (1.048576  * $duration))
    $bitrate = $bitrate - 32000 #subtract off audio bitrate
    #Piece together the file name
    $filename = $_.Name.Split(".")[0] + "-two-pass.webm"

    #Do two pass encode to make a decent quality but low filesize webm
    ffmpeg.exe -y -i $_.Name `
        -c:v libvpx-vp9 -b:v $bitrate -row-mt 1 -pass 1 -deadline good`
        -c:a libopus -ac 1 -b:a 32k -vbr on -compression_level 10 -frame_duration 60 -application audio `
        -f webm NUL

    ffmpeg.exe -y -i $_.Name `
        -c:v libvpx-vp9 -b:v $bitrate -row-mt 1 -pass 2 -deadline good`
        -c:a libopus -ac 1 -b:a 32k -vbr on -compression_level 10 -frame_duration 60 -application audio `
        -f webm $filename
}

Read-Host -Prompt "Press any key to continue"
